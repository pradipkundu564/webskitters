import { object, string, ref } from "yup";

export const createUserSchema = object({
    body: object({
        email: string()
            .email("Must be a valid email")
            .required("Email is required"),
        password: string()
            .required("Password is required")
            .min(6, "Password is too short - should be 6 chars minimum."),
        mobile: string()
            .required("Mobile number is required"),
        name: string()
        .required("Name is required"),
        image: string()
        .required("Image is required"),
    })
})
