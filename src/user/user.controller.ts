import { Request, Response } from "express";
import { omit } from "lodash";
import { createUser, findUser } from "./user.service";
import log from "../logger";
import { get } from "lodash";
import mongoose from "mongoose";
import multer from "multer";
import path from "path";

export async function createUserHandler(req: Request, res: Response){
    try {
        const user = await createUser(req.body);
        return res.send(omit(user.toJSON(), "password"));
    } catch(err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}



export const addImageHandler = async (
    req: Request,
    res: Response
) => {
    var filename = Date.now() + ".jpg";
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "uploads")
        },
        filename: function (req, file, cb) {
            cb(null, filename)
        }
    })

    const maxSize = 100 * 1000 * 1000;

    var upload = multer({
        storage: storage,
        limits: { fileSize: maxSize },
        fileFilter: function (req, file, cb) {

            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(file.mimetype);

            var extname = filetypes.test(path.extname(
                file.originalname).toLowerCase());
            if (mimetype && extname) {
                return cb(null, true);
            }
        }

    }).single("image");

    upload(req, res, function (err: any) {

        res.send(filename);
    })
};




export async function getUserHandler(req: Request, res: Response){
    try {
        //@ts-ignore
        const user: any = await findUser(new mongoose.Types.ObjectId(req.user._id));
        return res.send(omit(user.toJSON(), "password"));
    } catch(err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}