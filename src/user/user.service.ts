import { omit } from "lodash";
import { DocumentDefinition, FilterQuery, ObjectId } from "mongoose";
import User, { UserDocument } from "./user.model";

export async function createUser(userInput: DocumentDefinition<UserDocument>) {
    try {
        return await User.create(userInput);
    } catch (err: any) {
        throw new Error(err);
    }
}



export async function findUser(query: FilterQuery<UserDocument>) {
    return User.findOne(query);
}

export async function validatePassword({
    email,
    password,
}: {
    email: UserDocument["email"];
    password: string;
}) {
    const user = await User.findOne({ email });

    if (!user) {
        return false;
    }

    const isValid = await user.comparePassword(password);

    if (!isValid) {
        return false;
    }

    return omit(user.toJSON(), "password");
}