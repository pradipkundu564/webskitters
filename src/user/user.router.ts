import express, { Express, Request, Response } from "express";
import validateRequset from "../middleware/validateRequest";
import { addImageHandler } from "./user.controller";
import { createUserHandler, getUserHandler } from "./user.controller";
import { createUserSchema } from "./user.schema";
import requiresUser from "../middleware/requiresUser";
const router = express.Router();

router.post("/", validateRequset(createUserSchema), createUserHandler);
router.post('/image', addImageHandler);
router.get("/", requiresUser, getUserHandler);

export default router;
