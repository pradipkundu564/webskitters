import { Express, Request, Response } from "express";
import postRouter from "./posts/post.router";
import sessionRouter from "./session/session.router";
import userRouter from "./user/user.router";


export default function(app: Express) {
    app.get(["/", "/healthCheck"], (req: Request, res: Response) => res.sendStatus(200));
    app.use('/api/user', userRouter);
    app.use('/api/post', postRouter);
    app.use('/api/session', sessionRouter);
}