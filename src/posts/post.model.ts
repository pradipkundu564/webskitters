import mongoose from "mongoose";
import { UserDocument } from "../user/user.model";

export interface postDocument extends mongoose.Document {
  user: UserDocument["_id"];
  title: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
}

const postSchema = new mongoose.Schema(
  {
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    title: { type: String, required: true },
    description: { type: String }
  },
  { timestamps: true }
);

const post = mongoose.model<postDocument>("post", postSchema);

export default post;