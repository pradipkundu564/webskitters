import express, { Express, Request, Response } from "express";
import requiresUser from "../middleware/requiresUser";
import validateRequset from "../middleware/validateRequest";
import { createPostHandler, getPostHandler } from "./post.controller";
import { createPostSchema } from "./post.schema";
const router = express.Router();

router.post('/', validateRequset(createPostSchema), requiresUser, createPostHandler);
router.get('/', requiresUser, getPostHandler);

export default router;
