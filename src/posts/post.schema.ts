import { object, string, ref, array } from "yup";

export const createPostSchema = object({
    body: object({
        title: string()
            .required("Title is required"),

        description: string()
            .max(100, "Description is too long - should be 100 chars maximum."),
    }),

});