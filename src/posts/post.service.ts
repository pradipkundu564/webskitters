import { omit } from "lodash";
import { DocumentDefinition, FilterQuery, QueryOptions, UpdateQuery } from "mongoose";
import post, { postDocument } from "./post.model";

export async function createpost(postInput: DocumentDefinition<postDocument>) {
    try {
        return await post.create(postInput);
    } catch (err: any) {
        throw new Error(err);
    }
}


export async function findposts(query: FilterQuery<postDocument>) {
    return post.find(query).lean();
}


export async function findAndUpdate(query: FilterQuery<postDocument>, update: UpdateQuery<postDocument>, options: QueryOptions) {
    return await post.findOneAndUpdate(query, update, options);
}

export async function findAndDelete(query: FilterQuery<postDocument>) {
    return await post.deleteOne(query);
}