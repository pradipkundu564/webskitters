import log from "../logger";
import { Request, Response } from "express";
import { omit, get } from "lodash";
import { createpost, findAndUpdate, findposts, findAndDelete } from "./post.service";
import multer from "multer";
import path from "path";
import fs from "fs"

export async function createPostHandler(req: Request, res: Response) {
    try {
        //@ts-ignore
        req.body.user = get(req, "user._id");
        const post = await createpost(req.body);
        return res.send(omit(post.toJSON(), "password"));
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}

export async function getPostHandler(req: Request, res: Response) {
    try {
        //@ts-ignore
        const userId = get(req, "user._id");
        const posts = await findposts({ user: userId });
        return res.send(posts);
    } catch (err: any) {
        log.error(err);
        return res.status(400).send(err.message);
    }
}
