import { get } from "lodash";
import { Request, Response, NextFunction } from "express";
import multer from "multer";
import path from "path";

const addImage = async (
    req: Request,
    res: Response
) => {
    var filename = Date.now() + ".jpg";
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "uploads")
        },
        filename: function (req, file, cb) {
            cb(null, filename)
        }
    })

    const maxSize = 100 * 1000 * 1000;

    var upload = multer({
        storage: storage,
        limits: { fileSize: maxSize },
        fileFilter: function (req, file, cb) {

            var filetypes = /jpeg|jpg|png/;
            var mimetype = filetypes.test(file.mimetype);

            var extname = filetypes.test(path.extname(
                file.originalname).toLowerCase());
            if (mimetype && extname) {
                return cb(null, true);
            }
        }

    }).single("image");

    upload(req, res, function (err: any) {

        res.send(filename);
    })
};

export default addImage;